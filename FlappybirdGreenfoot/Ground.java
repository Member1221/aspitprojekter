import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Ground here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Ground extends GameActor
{
    ScrollUtils _scroller;
    private GreenfootImage _groundSprite = new GreenfootImage("ground.png");
    private float _baseScrollSpeed = 1f;
    
    public Ground()
    {
        _scroller = new ScrollUtils(getImage().getWidth(), getImage(), _groundSprite);
        _scroller.ScrollSpeed = 1f;
    }
    
    /**
     * Act - do whatever the Ground wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (!((MyWorld)getWorld()).BIRD.IsDead && MyWorld.STARTED)
        {
            _scroller.ScrollSpeed = _baseScrollSpeed * MyWorld.LEVEL;
            _scroller.scrollImg();
        }
    }
}
