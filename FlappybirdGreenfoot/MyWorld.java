import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)
import java.util.*;

/**
 * Write a description of class MyWorld here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class MyWorld extends World
{
    private int _bgw = 0;
    private int _bgwsw = 0;
    private int _bgws = 0;
    private float _scroll = 0;
    private float _scrollspeed = 0.3f;
    private int pipeTimeout = 0;
    private int pipeTimeoutMax = 200;//240;
    
    private GreenfootImage _bgImg = new GreenfootImage("flappy_background.png");
    
    public Bird BIRD;
    public Ground GROUND;
    public GameOver GAME_OVER = null;
    
    public static boolean STARTED = false;
    
    public static int LEVEL = 1;
    
    public static int SCORE = 0;
    public static int HIGH_SCORE = 0;
    /**
     * Constructor for objects of class MyWorld.
     * 
     */
    public MyWorld()
    {
        // Create a new world with 600x400 cells with a cell size of 1x1 pixels.
        super(600, 400, 1, false);
        SCORE = 0;
        _bgwsw = getBackground().getWidth();
        _bgws = (getWidth()/_bgwsw)+6;
        _bgw = _bgImg.getWidth();
        prepare();
        bgscroll();
    }

    public void act()
    {
        if (STARTED)
            bgscroll();
        if (!BIRD.IsDead)
        {
            if (STARTED)
            {
                List<Pipe> ga = getObjects(Pipe.class);
                for (int i = 0; i < ga.size(); i++)
                {
                    if (ga.get(i).IsDead)
                    {
                        removeObject(ga.get(i));
                    }
                }
                pipeTimeout++;
                if (pipeTimeout > (pipeTimeoutMax + (LEVEL/pipeTimeoutMax)))
                {
                    Pipe mPipe = new Pipe();
                    int p = Greenfoot.getRandomNumber(200);
                    p = -(p/2);
                    addObject(mPipe, getWidth()+128, p);
                    addObject(new Pipe(mPipe), getWidth(), 300);
                    setPaintOrder(Ground.class, ScoreCounter.class);
                    pipeTimeout = 0;
                }
            }
        }
        else
        {
            if (GAME_OVER == null)
            {
                GAME_OVER = new GameOver();
                addObject(GAME_OVER,295,188);
                GAME_OVER.init();
                
                addObject(new GUIButton("button_restart.png", new GUICallback()
                {
                    public void Run() {
                        reset();
                    }
                }),295,250);
            }
        }
    }

    public void bgscroll()
    {
        if (!BIRD.IsDead)
        {
            _scroll += _scrollspeed*LEVEL;

            for (int i = 0; i < _bgws; i++)
            {
                int x = (i*(_bgw-(_bgw*2)))+(_bgwsw+_bgw);
                getBackground().drawImage(_bgImg, x-(int)_scroll, 0);
            }
            if (_scroll > _bgw)
            {
                _scroll = 0;
            }
        }
    }
    
    private void reset()
    {
        List<Actor> ga = getObjects(null);
        for (int i = 0; i < ga.size(); i++)
        {
            removeObject(ga.get(i));
        }
        GAME_OVER = null;
        STARTED = false;
        HIGH_SCORE = SCORE;
        SCORE = 0;
        LEVEL = 1;
        prepare();
    }
    
    /**
     * Prepare the world for the start of the program.
     * That is: create the initial objects and add them to the world.
     */
    private void prepare()
    {
        BIRD = new Bird();
        addObject(BIRD,61,199);
        BIRD.setLocation(110,197);

        GROUND = new Ground();
        addObject(GROUND,298,383);

        
        ScoreCounter scorecounter = new ScoreCounter();
        addObject(scorecounter,32,32);
    }
}
