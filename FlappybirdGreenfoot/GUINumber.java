import greenfoot.*;

/**
 * A class to create Graphical User Interface numbers via a spritesheet.
 * 
 * @author Theis Nielsen
 * @version 1.0
 * @license GPL
 */
public class GUINumber implements IGUI
{
    // instance variables - replace the example below with your own
    private int _val;
    private int _row;
    private int _rows;
    private boolean _plus;
    private static GreenfootImage _sheet = new GreenfootImage("numtable.png");
    
    /**
     * Constructor for objects of class GUINumber
     */
    public GUINumber(boolean plus, int number)
    {
        this(plus, number, 0);
    }
    
    /**
     * Constructor for objects of class GUINumber. 
     * 2 Rows default value.
     */
    public GUINumber(boolean plus, int number, int row)
    {
        this(plus, number, 2, 0);
    }
    
    public GUINumber(boolean plus, int number, int rows, int row)
    {
        _val = number;
        _rows = rows;
        _row = row;
        _plus = plus;
    }
    
    /**
     * Returns the image assoiciated with this GUINumber.
     * Returns null if getting the image fails.
     */
    public GreenfootImage getGUIImage()
    {
        String value = String.valueOf(_val);
        int w = _sheet.getWidth()/_rows;
        int h = _sheet.getHeight()/10;
        GreenfootImage tmp = new GreenfootImage(w, h);
        int len = value.length();
        if (_plus)
        {
            value = "+" + value;
            len++;
        }
        GreenfootImage output = new GreenfootImage(len*(w), h-1);
        int ci = 0;
        for (char i : value.toCharArray())
        {
            tmp.clear();
            int val = 0;
            int normr = _row;
            if (i == '-')
            {
                val = 10;
            }
            else if (i == '+')
            {
                val = 10;
                _row = 1;
            }
            else
            {
                val = Character.getNumericValue(i);
            }
            tmp.drawImage(_sheet, -_row*w, -val*(h-1));
            output.drawImage(tmp, ci*w,0);
            _row = normr;
            ci++;
        }
        return output;
    }
}
