/**
 * A callback for running code when an GUI state requests it.
 */
public interface GUICallback  
{
    void Run();
}
