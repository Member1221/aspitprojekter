import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class ScoreCounter here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ScoreCounter extends GameActor
{
    Font _font = new Font("Impact", false, false, 12);
    GreenfootImage _scoreImg;
    
    
    public ScoreCounter()
    {
        _scoreImg = new GreenfootImage(512, 512);
        act();
    }
    
    public void act() 
    {
        setImage(new GUINumber(false, MyWorld.SCORE).getGUIImage());
    }    
}
