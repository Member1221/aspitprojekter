import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Pipe here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Pipe extends GameActor
{
    private Pipe _master = null;
    private boolean _givenPoint = false;
    
    public Pipe(Pipe _master)
    {
        this._master = _master;
        setImage("bottom_pipe.png");
    }
    
    public Pipe()
    {
        this._master = null;
        if (_master == null)
        {
            setImage("top_pipe.png");
        }
    }
    
    private int counter;
    
    public int getLevel()
    {
        counter++;
        if (counter > 11)
            counter = 1;
        return counter;
    }
    
    public void act() 
    {
        if (MyWorld.STARTED)
        {
            if (!((MyWorld)getWorld()).BIRD.IsDead)
            {
                if (intersects(((MyWorld)getWorld()).BIRD))
                {
                    ((MyWorld)getWorld()).BIRD.IsDead = true;
                }
                if (_master != null && !_master.IsDead)
                {
                    setLocation(_master.getX(), _master.getY()+390);
                    getImage().setTransparency(_master.getImage().getTransparency());
                
                    return;
                }
                setLocation(getX()-(1*MyWorld.LEVEL), getY());
            
        
                if (80 > getX())
                {
                    if (_master == null)
                    {
                        if (!_givenPoint)
                        {
                            MyWorld.SCORE++;
                            if (getLevel() >= 10)
                                MyWorld.LEVEL++;
                            _givenPoint = true;
                        }
                    }
                    int t = getX()+128;
                    if (t < 0)
                    {
                        t = 0;
                        IsDead = true;
                    }
                    return;
                }
            }
            else
            {
                if (intersects(((MyWorld)getWorld()).BIRD))
                {
                    ((MyWorld)getWorld()).BIRD.pushBack();
                }
            }
        }
    }    
}
