import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class GameOver here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GameOver extends GameActor
{
    private int _visibility = 0;
    private int _endY = 0;
    private float _locY = 0;
    
    public void init()
    {
        getImage().setTransparency(_visibility);
        _endY = getY();
        setLocation(getX(), _endY-16);
        _locY = getY();
    }
    
    
    
    public void act() 
    {
        if (_visibility != 255)
        {
            _locY += (16f/255f);
            _visibility++;
            
            setLocation(getX(), (int)_locY);
            getImage().setTransparency(_visibility);
        }
    }    
}
