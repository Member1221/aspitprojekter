import greenfoot.*;
/**
 * Write a description of class ScrollUtils here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class ScrollUtils  
{
    float _scroll;
    public float ScrollSpeed;
    private int _bgw = 0;
    private int _bgwsw = 0;
    private int _bgws = 0;
    
    GreenfootImage dImg;
    GreenfootImage img;
    
    public ScrollUtils(int width, GreenfootImage img, GreenfootImage dImg)
    {
        this.img = img;
        this.dImg = dImg;
        _bgwsw = img.getWidth();
        _bgws = (width/_bgwsw)+6;
        _bgw = dImg.getWidth();
    }
    
    public void scrollImg()
    {
        _scroll += ScrollSpeed;
        
        for (int i = 0; i < _bgws; i++)
        {
            int x = (i*(_bgw-(_bgw*2)))+(_bgwsw+_bgw);
            img.drawImage(dImg, x-(int)_scroll, 0);
        }
        if (_scroll > _bgw)
        {
            _scroll = 0;
        }
    }
}
