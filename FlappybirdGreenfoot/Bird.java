import greenfoot.*;  // (World, Actor, GreenfootImage, Greenfoot and MouseInfo)

/**
 * Write a description of class Bird here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class Bird extends GameActor
{
    private float _gravity = .1f;
    private float _yVelocity = 0f;
    private float _terminalVelocity = 5f;
    private boolean _wasKeyDown = false;
    
    private int _flapFrames = 2;
    private int _flapFrame = -1;
    private int _flapFrameT = 0;
    private int _flapFrameTMax = 20;
    
    /**
     * Act - do whatever the Bird wants to do. This method is called whenever
     * the 'Act' or 'Run' button gets pressed in the environment.
     */
    public void act() 
    {
        if (MyWorld.STARTED)
        {
            if (!isCollidingGround())
            {
                handleGravity();
            }
            else
            {
                IsDead = true;
            }
        }
        handleLookAt();
        if (!IsDead)
        {
            if (_flapFrame > -1)
            {
                //Flap the birds wings
                handleFlapFlap();
            }
            else
            {
                setImage("flappybird2.png");
            }
            if ( wantsToJump() )
            {
                flap();
                if (!MyWorld.STARTED)
                    MyWorld.STARTED = true;
                _wasKeyDown = true;
            }
            if ( wantsNotToJump() )
            {
                _wasKeyDown = false;
            }
        }
    }
    
    public boolean isCollidingGround()
    {
        if (intersects(((MyWorld)getWorld()).GROUND))
            return true;
        return false;
    }
    
    public void handleFlapFlap()
    {
        if (_flapFrameT > _flapFrameTMax)
        {
            //Update frame!
            _flapFrame++;
            //Update THE BIRD
            if (_flapFrame > 3)
            {
                //Stop flapping
                _flapFrame = -1;
                return;
            }
            setImage("flappybird" + (int)(_flapFrame) + ".png");
            _flapFrameT = 0;
        }
        _flapFrameT++;
    }
    
    public void handleLookAt()
    {
         float t = _yVelocity;
         boolean n = false;
         if (t < 0)
         {
             n = true;
             t = -t;
             if (t > _terminalVelocity)
                t = _terminalVelocity;
         }
         
         float percent = t/_terminalVelocity;
         float val = 90*percent;
         if (!n)
            setRotation((int)val);
         else
            setRotation(359-(int)val);
    }

    public void pushBack()
    {
        MyWorld.SCORE--;
        _yVelocity = 0;
        _yVelocity -= (_terminalVelocity-(_terminalVelocity/8));
        setLocation(getX()-15, getY()+(int)_yVelocity);
    }
    
    public void flap()
    {
        _yVelocity = 0;
        _yVelocity -= (_terminalVelocity-(_terminalVelocity/4));
        setLocation(getX(), getY()+(int)_yVelocity);
        _flapFrame = 0;
    }
    
    public void handleGravity()
    {
        _yVelocity += _gravity;
        if (_yVelocity > _terminalVelocity)
        {
            _yVelocity = _terminalVelocity;
        }
        if (getY() < 0)
            _yVelocity = 1f;
        setLocation(getX(), getY()+(int)_yVelocity);
    }
    
    public boolean wantsNotToJump()
    {
        if (_wasKeyDown && !isJumpKeyDown())
            return true;
        return false;
    }
    
    public boolean wantsToJump()
    {
        if (!_wasKeyDown && isJumpKeyDown())
            return true;
        return false;
    }
    
    public boolean isJumpKeyDown()
    {
        if (Greenfoot.isKeyDown("space") || Greenfoot.isKeyDown("w") || Greenfoot.isKeyDown("up"))
            return true;
        return false;
    }
    
}
