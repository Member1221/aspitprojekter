import java.util.*;
import greenfoot.*;

/**
 * Write a description of class GUIButton here.
 * 
 * @author (your name) 
 * @version (a version number or a date)
 */
public class GUIButton extends Actor implements IGUI
{
    private GreenfootImage _button;
    private GUICallback _callback;
    private boolean _mouseState = false;
    private boolean _mouseRel = false;
    private int _state = 1;
    
    
    public GUIButton(String image, GUICallback callback)
    {
        _button = new GreenfootImage(image);
        setImage(new GreenfootImage(_button.getWidth(), _button.getHeight()/3));
        _callback = callback;
    }
    
    public void act()
    {
        if (mouseInBounds())
        {
            clickHandle();
            if (_mouseState)
            {
                _state = 2;
            }
            else
            {
                _state = 1;
            }
        }
        else
        {
            _state = 0;
        }
        setImage(getGUIImage());
        if (_mouseRel && _callback != null)
            _callback.Run();
    }
    
    private void clickHandle() {
        if (Greenfoot.mousePressed(this))
        {
            _mouseState = true;
        }
        else if (Greenfoot.mouseClicked(this))
        {
            _mouseRel = true;
            _mouseState = false;
        }
    }
    
    public boolean mouseInBounds()
    {
        MouseInfo m = Greenfoot.getMouseInfo();
        if (m != null) {
            if (m.getX() >= (getX()-_button.getWidth()/2) && m.getX() <= getX() + _button.getWidth()/2)
            {
                if (m.getY() >= (getY()-((getImage().getHeight()/3))) && m.getY() <= getY() + (getImage().getHeight()/3))
                {
                    return true;
                }
            }
        }
        return false;
    }
    
    public GreenfootImage getGUIImage()
    {
        GreenfootImage tmp = getImage();
        tmp.clear();
        tmp.drawImage(_button, 0, -_state*(_button.getHeight()/3));
        return tmp;
    }
}
