﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace ConsoleApp1
{
    public class Bird : Element
    {
        private float gravity = .3f;
        private float yVelocity = 0f;
        private float terminalVelocity = 5f;
        private KeyboardState lastState;

        public Bird(Vector2 position, World w)
            : base("bird", w, position, new Vector2(32, 32), 0, new FlappyTexture(3, 2, Mode.Day, "flapbird"))
        {
            
        }

        public override void Init()
        {

        }

        public override void Update(GameTime gameTime)
        {
            if (FlappyGame.STARTED)
            {
                if (!IsCollidingId("ground"))
                    HandleGravity();

                base.Update(gameTime);

                if (IsAlive)
                {
                    if (IsCollidingId("ground") || IsCollidingId("pipe"))
                    {
                        Flap();
                        Kill();
                        FlappyGame.GAME_OVER = true;
                        return;
                    }


                    if (ShouldJump())
                        Flap();
                }
                HandleLookAt();
                return;
            }
            if (ShouldJump())
            {
                FlappyGame.STARTED = true;
                Flap();
            }
        }

        public void HandleGravity()
        {
            yVelocity += gravity;
            if (yVelocity > terminalVelocity)
            {
                yVelocity = terminalVelocity;
            }
            if (pos.Y < 0)
                yVelocity = 1f;
            pos = new Vector2(pos.X, pos.Y + yVelocity);
            
        }

        public void HandleLookAt()
        {
            float t = yVelocity;
            bool n = false;
            if (t < 0)
            {
                n = true;
                t = -t;
                if (t > terminalVelocity)
                    t = terminalVelocity;
            }

            float percent = t / terminalVelocity;
            float val = 90 * percent;
            if (!n)
                SetRotation(MathHelper.ToRadians(val));
            else
                SetRotation(-MathHelper.ToRadians(val));
        }

        public bool ShouldJump()
        {
            var state = Keyboard.GetState();
            if ((state.IsKeyDown(Keys.Space) || state.IsKeyDown(Keys.W) || state.IsKeyDown(Keys.Up)) && (!lastState.IsKeyDown(Keys.Space) && !lastState.IsKeyDown(Keys.W) && !lastState.IsKeyDown(Keys.Up)))
            {
                lastState = state;
                return true;
            }
            lastState = state;
            return false;
        }

        public void Flap()
        {
            yVelocity = 0;
            yVelocity -= (terminalVelocity - (terminalVelocity / 6));
            pos = new Vector2(pos.X, pos.Y + (int)yVelocity);
            //flapFrame = 0;
        }

        public override void Draw(GameTime gameTime)
        {
            FlappyDrawer.Draw(this.texture, this.pos, this.rotation, null, 1, false, 1);
        }
    }
}
