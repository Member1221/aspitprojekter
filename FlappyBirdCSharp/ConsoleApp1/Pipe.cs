﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace ConsoleApp1
{
    public class Pipe : Element
    {
        public FlappyTexture TopTexture;

        public bool given = false;

        public Pipe(Vector2 position, World w)
            : base("pipe", w, position, new Vector2(52, 266), 0, new FlappyTexture(2, 1, Mode.Day, "flappipes"))
        {
            hitboxes.Add(new Rectangle((int)this.pos.X - ((texture.GetTexture().Width / 2)/2), (int)this.pos.Y - ((texture.GetTexture().Height / 2)) - 128, hitboxes[0].Width, texture.GetTexture().Height / 2));
            TopTexture = new FlappyTexture(2, 0, Mode.Day, "flappipes");
        }

        public override void Draw(GameTime gameTime)
        {

        }

        public override void PreDraw(GameTime gameTime)
        {
            FlappyDrawer.Draw(this.texture, new Vector2(hitboxes[0].X, hitboxes[0].Y), this.rotation, new Vector2(0, 0));
            FlappyDrawer.Draw(this.TopTexture, new Vector2(hitboxes[1].X, hitboxes[1].Y), this.rotation, new Vector2(0,0));
        }

        public override void Update(GameTime gameTime)
        {
            if (!FlappyGame.GAME_OVER)
            {
                pos = new Vector2(pos.X - 3, pos.Y);
                this.hitboxes[0] = new Rectangle((int)this.pos.X - ((texture.GetTexture().Width / 2)), (int)this.pos.Y, hitboxes[0].Width, texture.GetTexture().Height / 2);
                this.hitboxes[1] = new Rectangle((int)this.pos.X - ((texture.GetTexture().Width / 2)), (int)this.pos.Y - ((texture.GetTexture().Height / 2)) - 128, hitboxes[0].Width, texture.GetTexture().Height / 2);
            }
            if (pos.X < -64)
                World.MarkRemoval(this);
            if (pos.X < (World as GameWorld).BIRD.Position.X && !given)
            {
                FlappyGame.SCORE++;
                FlappyGame.THIS_GAME.Window.Title = "SCORE: " + FlappyGame.SCORE;
                given = true;
            }
        }

        public override void Init()
        {

        }
    }
}
