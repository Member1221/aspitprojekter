﻿using System;
using System.Collections.Generic;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace ConsoleApp1
{
    class FlappyGame : Game
    {


        #region Properties

        private World world;
        private GraphicsDeviceManager graphics;

        // THIS CODE IS KINDA HACKY, NORMALLY I'D USE A COMPONENT HANDLER, BUT THAT'D TAKE TOO LONG TO SET UP.
        /// <summary>
        /// The current game.
        /// </summary>
        public static FlappyGame THIS_GAME;


        public static int SCORE = 0;
        public static bool GAME_OVER = false;
        public static bool STARTED = false;
        #endregion


        #region Getters/Setters
        /// <summary>
        /// The graphics handler
        /// </summary>
        public GraphicsDeviceManager Graphics
        {
            get { return graphics; }

            //Leave this empty so graphics doesn't get overwritten.
            set { }
        }

        /// <summary>
        /// The spritebatch
        /// </summary>
        public SpriteBatch SpriteBatch
        {
            get;
            set;
        }
        #endregion


        #region Constructors
        public FlappyGame()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            //Window.AllowUserResizing = true;
            this.IsMouseVisible = true;
            THIS_GAME = this;
        }
        #endregion


        #region Methods
        protected override void OnExiting(object sender, EventArgs args)
        {
            base.OnExiting(sender, args);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            Window.Title = "Flappy Bird Clone";
            // TODO: Add your initialization logic here
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            SpriteBatch = new SpriteBatch(GraphicsDevice);

            bgtx = Content.Load<Texture2D>("flapbgday");
            grtx = Content.Load<Texture2D>("flapground");
            Counter.LoadTextures("flapnumtable");
            GroundTextureHeight = grtx.Height/2;

            //Initialize the world.
            world = new GameWorld().Init();

            //TODO: use this.Content to load your game content here 
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
            if (Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            //Fun little thing
            //mv = 0+(1 * ((Window.ClientBounds.X + Window.ClientBounds.Width) - Mouse.GetState().Position.X));

            //Advance background
            if (!GAME_OVER && STARTED)
                mv +=1;

            world.WorldUpdate(gameTime);
        }


        private Texture2D bgtx;
        private Texture2D grtx;
        private int mv = 0;
        public int GroundTextureHeight;
        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            graphics.GraphicsDevice.Clear(Color.Black);

            //Allow drawing the background as a scrolling 8bit texture.
            SpriteBatch.Begin(SpriteSortMode.BackToFront, null, SamplerState.LinearWrap, null, null, null, null);
            SpriteBatch.Draw(bgtx, new Rectangle(0, 0, Window.ClientBounds.Width, Window.ClientBounds.Height), new Rectangle(mv/6, 0, Window.ClientBounds.Width, bgtx.Height), Color.White);
            SpriteBatch.End();


            SpriteBatch.Begin(SpriteSortMode.BackToFront, null, SamplerState.PointClamp, null, null, null, null);
            world.WorldPreDraw(gameTime);
            SpriteBatch.End();

            SpriteBatch.Begin(SpriteSortMode.BackToFront, null, SamplerState.LinearWrap, null, null, null, null);
            SpriteBatch.Draw(grtx, new Rectangle(0, Window.ClientBounds.Height - (grtx.Height/2), Window.ClientBounds.Width, grtx.Height/2), new Rectangle(mv*3, 0/*grtx.Height / 2*/, Window.ClientBounds.Width, grtx.Height / 2), Color.White);
            SpriteBatch.End();

            SpriteBatch.Begin(SpriteSortMode.BackToFront, null, SamplerState.PointClamp, null, null, null, null);
            world.WorldDraw(gameTime);
            Counter.DrawNumber(SCORE, new Vector2(Window.ClientBounds.Width / 2, 32));
            SpriteBatch.End();
            //Allow draing sprites as 8 bit pixellated sprites.

            base.Draw(gameTime);
        }
        #endregion


    }
}
