﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public class FlappyDrawer
    {
        /// <summary>
        /// Draws a flappysprite
        /// </summary>
        /// <param name="texture">The texture</param>
        /// <param name="Position">Position in world</param>
        /// <param name="rotation">Rotation</param>
        /// <param name="Origin">Origin of rotation</param>
        /// <param name="scale">Scale</param>
        /// <param name="flip">should the sprite flip horizontally?</param>
        public static void Draw(FlappyTexture texture, Vector2 Position, float rotation, Vector2? Origin = null, float scale = 1f, bool flip = false, float layer = 0)
        {
            if (texture != null)
            {
                Vector2 origin = Vector2.Zero;
                if (Origin == null)
                    origin = new Vector2((texture.GetTexture().Width / 2) / 2, (texture.GetTexture().Height / texture.GetFrameCount()) / 2);
                else
                    origin = (Vector2)Origin;

                if (!flip)
                    FlappyGame.THIS_GAME.SpriteBatch.Draw(texture.GetTexture(), Position, texture.GetFrame(), Color.White, rotation, origin, scale, SpriteEffects.None, layer);
                else
                    FlappyGame.THIS_GAME.SpriteBatch.Draw(texture.GetTexture(), Position, texture.GetFrame(), Color.White, rotation, origin, scale, SpriteEffects.FlipHorizontally, layer);
            }
        }

        public static void Draw(FlappyTexture texture, Rectangle size)
        {
            if (texture != null)
            {
                FlappyGame.THIS_GAME.SpriteBatch.Draw(texture.GetTexture(), size, Color.White);
            }
        }
    }
}
