﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    /// <summary>
    /// The mode of the texture (Day, Night)
    /// </summary>
    public enum Mode
    {
        Day = 0,
        Night = 1
    }

    public enum MovementMode
    {
        Forwards,
        Backwards
    }

    public class FlappyTexture
    {
        private static Dictionary<string, Texture2D> textureBank = new Dictionary<string, Texture2D>();

        //The underlying texture.
        private string texture;
        private int frames;

        private MovementMode moveMode;
        private Mode mode;
        private int frame;
        private int framePlayCount = 0;
        private int startFrame = 0;
        private bool animated;

        /// <summary>
        /// Creates a new FlappyTexture. 
        /// </summary>
        /// <param name="frames">The amount of frames (vertical sprites) there is.</param>
        /// <param name="frame">The start frame</param>
        /// <param name="mode">The start sprite mode</param>
        /// <param name="textureName">The underlying Texture2D</param>
        public FlappyTexture(int frames, int frame, Mode mode, string textureName, MovementMode mmode = MovementMode.Forwards, bool animated = true)
        {
            //Loads the texture into the texture bank if it is not loaded yet.
            if (!textureBank.ContainsKey(textureName))
                textureBank.Add(textureName, FlappyGame.THIS_GAME.Content.Load<Texture2D>(textureName));

            this.texture = textureName;
            this.frames = frames;
            this.frame = frame;
            this.startFrame = frame;
            this.mode = mode;
            this.moveMode = mmode;
            this.animated = animated;
        }

        /// <summary>
        /// Sets the current frame to be showed.
        /// </summary>
        /// <param name="frame"></param>
        public void SetFrame(int frame)
        {
            this.frame = frame;
            if (frame < 0)
                frame = 0;
            if (frame >= frames)
                frame = frames - 1;
        }

        /// <summary>
        /// Resets the animation to the start frame.
        /// </summary>
        public void ResetFrame()
        {
            framePlayCount = 0;
            frame = startFrame;
        }

        /// <summary>
        /// Pushes the next frame in the animation (if animation is enabled)
        /// </summary>
        public void PushFrame()
        {
            if (animated)
            {
                if (moveMode == MovementMode.Forwards)
                    SetFrame(frame + 1);
                else
                    SetFrame(frame - 1);

                framePlayCount++;
                if (framePlayCount > frames)
                {
                    framePlayCount = 0;
                    frame = startFrame;
                }
            }
        }

        /// <summary>
        /// Gets the current frame
        /// </summary>
        /// <returns>The frame as a rectangle inside the origin Texture.</returns>
        public Rectangle GetFrame()
        {
            int x = 0;
            int y = 0;
            int w = GetTexture().Width/2;
            int h = GetTexture().Height/frames;
            x = ((int)mode) * w;
            y = frame * h;
            return new Rectangle(x, y, w, h);
        }

        public int GetFrameCount()
        {
            return frames;
        }

        public Texture2D GetTexture()
        {
            return GetTexture(texture);
        }

        public static Texture2D GetTexture(string texture)
        {
            if (!textureBank.ContainsKey(texture))
                return null;
            return textureBank[texture];
        }
    }
}
