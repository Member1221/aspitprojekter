﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace ConsoleApp1
{
    public class GameWorld : World
    {
        private int droptimeout = 0;
        private int dropmtimeout = 150;
        public Bird BIRD;
        
        public override World Init()
        {
            base.Init();

            Add(new GroundCollider(this));
            BIRD = new Bird(new Vector2(125, FlappyGame.THIS_GAME.Window.ClientBounds.Height/2-16), this);
            Add(BIRD);

            return this;
        }

        Random height = new Random();
        public override void Update(GameTime gameTime)
        {
            if (!FlappyGame.GAME_OVER && FlappyGame.STARTED)
            {
                droptimeout++;
                int h = 150;
                if (droptimeout > dropmtimeout)
                {
                    
                    Add(new Pipe(new Vector2(FlappyGame.THIS_GAME.Window.ClientBounds.Width + 64, 250+(height.Next(0, h)-(h/2)+50)), this));
                    droptimeout = 0;
                }
            }
        }

        public override void Draw(GameTime gameTime)
        {

        }

        public override void PreDraw(GameTime gameTime)
        {

        }
    }
}
