﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    public abstract class Element
    {


        #region Properties

        protected FlappyTexture texture;
        protected Vector2 pos;
        protected float rotation;
	    protected List<Rectangle> hitboxes;
        private World world;
        private bool alive;
        private string id = "";
        #endregion


        #region Constructors

        public Element(string id, World w, Vector2 pos, Vector2 size, float rotation = 0f, FlappyTexture texture = null)
        {
            this.world = w;
            this.pos = pos;
            this.id = id;
            this.rotation = rotation;
            this.texture = texture;
            this.alive = true;
            this.hitboxes = new List<Rectangle>();
            this.hitboxes.Add(new Rectangle((int)pos.X, (int)pos.Y, (int)size.X, (int)size.Y));
            Console.WriteLine("Element " + id + " was added!...");
        }

        #endregion


        #region Getters/Setters

        public World World
        {
            get { return world; }
            set { }
        }

        public Vector2 Position
        {
            get { return pos; }
            set { pos = value; }
        }

        public List<Rectangle> Hitboxes
        {
            get { return hitboxes; }
            set { }
        }

        public bool IsAlive
        {
            get { return alive; }
            set { }
        }

        public string Id
        {
            get { return id; }
            set { }
        }

        #endregion


        #region Methods

        /// <summary>
        /// Update function for element. 
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Update(GameTime gameTime)
	    {
		    this.hitboxes[0] = new Rectangle((int)pos.X-hitboxes[0].Width/2, (int)pos.Y - hitboxes[0].Height / 2, hitboxes[0].Width, hitboxes[0].Height);
	    }

        /// <summary>
        /// Draw function for element
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void Draw(GameTime gameTime)
        {
            FlappyDrawer.Draw(this.texture, this.pos, this.rotation);
        }



        /// <summary>
        /// Init function for element
        /// </summary>
        public abstract void Init();

        /// <summary>
        /// Rotates object in the desired direction
        /// </summary>
        /// <param name="direction">The rotation applied in the direction</param>
        public void Rotate(float direction)
        {
            rotation += direction;
        }

        /// <summary>
        /// Sets the rotation of the object.
        /// </summary>
        /// <param name="direction">The rotation applied in the direction</param>
        public void SetRotation(float direction)
        {
            rotation = direction;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="b"></param>
        /// <returns></returns>
        public virtual bool IsColliding(Element b)
        {
            return World.IsColliding(this, b);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public virtual bool IsCollidingId(string id)
        {
            return World.IsCollidingWithAny(this, id);
        }

        /// <summary>
        /// You monster. (Marks this object as dead)
        /// </summary>
        public void Kill()
        {
            alive = false;
        }


        /// <summary>
        /// Predraw.
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual void PreDraw(GameTime gameTime)
        {
            //Do absolutely nothing
        }

        #endregion


    }
}
