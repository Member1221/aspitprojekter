﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Counter
    {
        public static FlappyTexture[,] Textures = new FlappyTexture[2, 11];

        public static void LoadTextures(string file)
        {
            for (int i = 0; i < 11; i++)
            {
                Textures[0, i] = new FlappyTexture(11, i, Mode.Day, file);
                Textures[1, i] = new FlappyTexture(11, i, Mode.Night, file);
            }
        }

        public static void DrawNumber(int num, Vector2 pos, bool addp = false, bool small = false)
        {
            string s = num.ToString();
            List<int> o = new List<int>();
            List<int> oi = new List<int>();

            int indexer = 0;
            if (small)
                indexer = 1;

            if (num < 0)
            {
                o.Add(11);
                oi.Add(0);   
            }
            else if (addp)
            {
                o.Add(10);
                oi.Add(1);
            }

            foreach (var c in s)
            {
                o.Add(int.Parse(c.ToString()));
                oi.Add(indexer);
            }

            int w = Textures[0, 0].GetTexture().Width / 2;
            for (int i = 0; i < o.Count; i++)
            {
                float offset = pos.X - ((i * w + (w / 2)) / 2);
                FlappyDrawer.Draw(Textures[oi[i], o[i]], new Vector2(offset + ((i * (w+(w/2)))), pos.Y), 0f);
            }
        }
    }
}
