﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Starting...");
            Console.Title = "Flappy Bird Clone v 1.0";
            using (var f = new FlappyGame())
            {
                f.Run();
            }
            Console.WriteLine("GameEnd!");
        }
    }
}
