﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    /// <summary>
    /// Represents the world where all elements run within.
    /// </summary>
    public abstract class World
    {


        #region Properties

        private List<Element> GameElements = new List<Element>();
        private List<Element> ForRemoval = new List<Element>();

        #endregion


        #region Constructors

        /// <summary>
        /// Empty constructor. Creates an empty, lonely world.
        /// </summary>
        public World()
        {

        }

        /// <summary>
        /// Constructor creating a element at creation
        /// </summary>
        /// <param name="elements"></param>
        public World(Element element)
        {
            GameElements.Add(element);
        }

        /// <summary>
        /// Constructor creating elements at creation
        /// </summary>
        /// <param name="elements"></param>
        public World(Element[] elements)
        {
            GameElements.AddRange(elements);
        }

        #endregion


        #region Methods


        /// <summary>
        /// Updates all elements in the world
        /// </summary>
        /// <param name="gameTime"></param>
        public void WorldUpdate(GameTime gameTime)
        {
            foreach (var element in GameElements)
                element.Update(gameTime);

            foreach (var element in ForRemoval)
            {
                GameElements.Remove(element);
                Console.WriteLine("Element " + element.Id + " was removed!...");
            }
            ForRemoval.Clear();

            Update(gameTime);
        }

        /// <summary>
        /// Draws all elements in the world
        /// </summary>
        /// <param name="gameTime"></param>
        public void WorldDraw(GameTime gameTime)
        {
            foreach (var element in GameElements)
                element.Draw(gameTime);

            Draw(gameTime);
        }

        /// <summary>
        /// Draws all elements in the world
        /// </summary>
        /// <param name="gameTime"></param>
        public void WorldPreDraw(GameTime gameTime)
        {
            foreach (var element in GameElements)
                element.PreDraw(gameTime);

            PreDraw(gameTime);
        }


        /// <summary>
        /// Draws all elements in the world
        /// </summary>
        /// <param name="gameTime"></param>
        public virtual World Init()
        {
            foreach (var element in GameElements)
                element.Init();
            return this;
        }

        /// <summary>
        /// Adds an element to the world.
        /// </summary>
        /// <param name="e"></param>
        public Element Add(Element e)
        {
            GameElements.Add(e);
            return e;
        }

        /// <summary>
        /// Marks an element for removal next destruction cycle.
        /// </summary>
        /// <param name="e"></param>
        public World MarkRemoval(Element e)
        {
            if (GameElements.Contains(e))
                ForRemoval.Add(e);
            return this;
        }

        public bool IsColliding(Element a, Element b)
        {
            foreach (var HitboxA in a.Hitboxes)
            {
                foreach (var HitboxB in b.Hitboxes)
                {
                    if (HitboxA.Intersects(HitboxB))
                        return true;
                }
            }
            return false;
        }

        public bool IsCollidingWithAny(Element a, string typeid, bool checker = false)
        {
            foreach (var e in GameElements)
                if (e.Id == typeid)
                {
                    foreach (var HitboxA in a.Hitboxes)
                    {
                        foreach (var HitboxB in e.Hitboxes)
                        {
                            if (HitboxA.Intersects(HitboxB))
                                return true;
                        }
                    }
                }
            return false;
        }

        public List<Element> GetWithId(string typeid)
        {
            List<Element> elements = new List<Element>();
            foreach (var e in GameElements)
                if (e.Id == typeid)
                    elements.Add(e);
            return elements;
        }

        /// <summary>
        /// Marks ALL elements in the world for destruction.
        /// </summary>
        /// <param name="e"></param>
        public World MarkArmageddon()
        {
            ForRemoval.AddRange(GameElements);
            return this;
        }

        public abstract void Update(GameTime gameTime);
        public abstract void Draw(GameTime gameTime);
        public abstract void PreDraw(GameTime gameTime);
        #endregion


    }
}
