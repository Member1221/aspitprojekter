﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;

namespace ConsoleApp1
{
    public class GroundCollider : Element
    {
        public GroundCollider(World w) 
            : base("ground",
                  w, 
                  new Vector2(0, FlappyGame.THIS_GAME.Window.ClientBounds.Height-(FlappyGame.THIS_GAME.GroundTextureHeight)+26), 
                  new Vector2(FlappyGame.THIS_GAME.Window.ClientBounds.Width, FlappyGame.THIS_GAME.GroundTextureHeight), 
                  0f, 
                  null)
        {
        }

        public override void Init()
        {
            
        }
    }
}
