<?php
include_once 'dbconn.php';

/**
 * This class represnts a user.
 */
class User
{
    //The user ID (UUID)
    private $user_id = "";

    //The user name (string)
    private $user_name = "";

    //Session.
    private $current_session;


    public function GetID()
    {
        return $this->user_id;
    }

    public function GetName()
    {
        return $this->user_name;
    }

    public static function NewUser($user_name, $passwd) {
        $r = $GLOBALS['DB_CONN']->RequestAllSpecific("nordicseaworld", "user", "username", $user_name);
        if (!isset($r['username']))
        {

            //Generate a unique id for the user
            $user_id = uniqid();

            //Hash their desired passwords, we want to make sure no one steals it :P
            $pss = password_hash($passwd, PASSWORD_DEFAULT);

            $r = $GLOBALS['DB_CONN']->InsertTo("nordicseaworld", "user", new DBRegisterCollumns(), new DBRegisterInsert($user_id, $user_name, $pss));

            $u = new User($user_id, $r['username']);
            $_SESSION['USER'] = $u;
            return $u;
        }
        else
        {
            //Nope out of there if the user already exists.
        }
    }

    public static function Login($user_name, $passwd) {
        $r = $GLOBALS['DB_CONN']->RequestAllSpecific("nordicseaworld", "user", "username", $user_name);
        if (isset($r['username']))
        {
            $password = $r['password_hash'];

            //Verify that the password is correct
            if (password_verify($passwd, $password))
            {
                //Actually log in! :D
                $u = new User($r['id'], $r['username']);
                $_SESSION['USER'] = $u;
                return $u;
            }
        }
        else
        {
            //Nope out of there if the user doesn't exists.
            die("User does not exist!");
        }
    }

    function __construct($user_id, $user_name) {

        //TODO: Create a session handle and set up other stuff.

        //Create a session to store temporary data.
        $this->current_session = session_start();

        //Declare the variables
        $this->user_id = $user_id;
        $this->user_name = $user_name;

    }
}

/**
 * DB Entry for registration
 */
class DBRegisterInsert extends DBInsert
{
    private $id = "";
    private $username = "";
    private $password_hash = "";

    public function __construct($id, $username, $password_hash)
    {
        $this->id = $id;
        $this->username = $username;
        $this->password_hash = $password_hash;
    }

    public function GetInsert()
    {
        $eid = mysql_escape_string($this->id);
        $eun = mysql_escape_string($this->username);
        $eps = mysql_escape_string($this->password_hash);
        return "'" . $eid . "', '" . $eun . "', '" . $eps . "'";
    }
}

/**
 * DB Entry for registration (collumns)
 */
class DBRegisterCollumns extends DBCollumns
{
    public function GetCollumns()
    {
        return "id, username, password_hash";
    }
}


?>
