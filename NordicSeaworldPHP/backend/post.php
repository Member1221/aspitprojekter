<?php
require_once 'dbconn.php';
require_once 'user.php';

/**
 * This class represents a post.
 */
class Post
{
    private $poster_id = "";
    private $post_id = "";
    private $post_html = "";

    public static function NewPost($postdata, $tag)
    {
        session_start();
        //Generate a unique id for the post
        $post_time = date_create()->format('Y-m-d H:i:s');
        $post_id = uniqid();
        $user_id = $_SESSION['USER']->GetID();
        $text_html = $postdata;

        $r = $GLOBALS['DB_CONN']->InsertTo("nordicseaworld", "posts", new DBPostCollumns(), new DBPostInsert($post_time, mysql_escape_string($tag), $post_id, $user_id, $text_html));

        return new Post($post_id, $user_id, $text_html);
    }

    public function GetPost($id)
    {
    }

    public function __construct($post_id, $poster_id, $post_html)
    {
        $this->post_id = $post_id;
        $this->poster_id = $poster_id;
        $this->post_html = $post_html;
    }

    function getPostHTML() {
        return $this->post_html;
    }
}

/**
 * DB Entry for posts
 */
class DBPostInsert extends DBInsert
{
    private $uuid = "";
    private $tag = "";
    private $posted_at = "";
    private $poster_id = "";
    private $text_html = "";

    public function __construct($at, $tag, $uuid, $pid, $text)
    {
        $this->uuid = $uuid;
        $this->tag = $tag;
        $this->posted_at = $at;
        $this->poster_id = $pid;
        $this->text_html = $text;
    }

    public function GetInsert()
    {
        $ept = mysql_escape_string($this->posted_at);
        $eid = mysql_escape_string($this->uuid);
        $etg = mysql_escape_string($this->tag);
        $epd = mysql_escape_string($this->poster_id);
        $eth = mysql_escape_string($this->text_html);
        return "'" . $eid . "', '" . $etg . "', '" . $ept .  "', '" . $epd . "', '" . $eth . "'";
    }
}

/**
 * DB Entry for posts (collumns)
 */
class DBPostCollumns extends DBCollumns
{
    public function GetCollumns()
    {
        return "uuid, tag, posted_at, poster_id, text_html";
    }
}

?>
