<?php
$GLOBALS['DB_CONN'] = DBConnection::LogIn("root", "localhost", "");

abstract class DBCollumns
{
    public abstract function GetCollumns();
}

abstract class DBInsert
{
    public abstract function GetInsert();
}

/**
 * Database connection.
 */
class DBConnection
{
    public $Connection;

    public static function LogIn($user_name, $server, $passwd)
    {
        $conn = new mysqli($server, $user_name, $passwd);
        if ($conn->connect_error) {
            die("Failed to connect to database!" . $conn->connect_error);
        }

        return new DBConnection($conn);
    }

    public function GetOrdered($db, $table, $item, $asc) {

        //Selects the database.
        $this->Connection->select_db($db);

        //Escape any sql queries inside the strings to make sure noone tries to inject bad code.
        $etable = mysql_escape_string($table);
        $eitem = mysql_escape_string($item);
        if ($asc == True) {
            $rows = $this->Connection->query("SELECT * FROM " . $etable . " ORDER BY " . $etable . "." . $eitem . " ASC");
        } else {
            $rows = $this->Connection->query("SELECT * FROM " . $etable . " ORDER BY " . $etable . "." . $eitem . " DESC");
        }
        if ($this->Connection->error)
        {
            die($this->Connection->error);
        }
        //Return it
        return $rows;
    }

    public function UpdateTo($db, $table, $item, $value, $id) {
        //Selects the database.
        $this->Connection->select_db($db);

        //Escape any sql queries inside the strings to make sure noone tries to inject bad code.
        $etable = mysql_escape_string($table);
        $eitem = mysql_escape_string($item);
        $evalue = $value;

        $rows = $this->Connection->query("UPDATE " . $table . " SET " . $eitem . "='" . $evalue . "' WHERE uuid='" . $id . "'");

        if ($this->Connection->error)
        {
            echo ($this->Connection->error);
            return false;
        }
        //Return it
        return true;
    }

    public function InsertTo($db, $table, $items, $values) {
        //Selects the database.
        $this->Connection->select_db($db);

        //Escape any sql queries inside the strings to make sure noone tries to inject bad code.
        $etable = mysql_escape_string($table);
        $eitem = $items->GetCollumns();
        $evalue = $values->GetInsert();

        $rows = $this->Connection->query("INSERT INTO " . $table . "(" . $eitem . ") VALUES (" . $evalue . ")");

        if ($this->Connection->error)
        {
            echo ($this->Connection->error);
            return false;
        }
        //Return it
        return true;
    }

    public function RequestSpecific($db, $table, $item, $value) {

            //Selects the database.
            $this->Connection->select_db($db);

            //Escape any sql queries inside the strings to make sure noone tries to inject bad code.
            $etable = mysql_escape_string($table);
            $eitem = mysql_escape_string($item);
            $evalue = mysql_escape_string($value);

            //Get the Data

            $rows = $this->Connection->query("SELECT " . $eitem . " FROM " . $etable . " WHERE " . $eitem . " = '" . $evalue . "'");
            if ($this->Connection->error)
            {
                die($this->Connection->error);
            }
            //Return it
            return $rows->fetch_assoc();
    }

    public function RequestAllSpecific($db, $table, $item, $value) {

            //Selects the database.
            $this->Connection->select_db($db);

            //Escape any sql queries inside the strings to make sure noone tries to inject bad code.
            $etable = mysql_escape_string($table);
            $eitem = mysql_escape_string($item);
            $evalue = mysql_escape_string($value);

            //Get the Data
            $rows = $this->Connection->query("SELECT * FROM " . $etable . " WHERE " . $eitem . " = '" . $evalue . "'");
            if ($this->Connection->error)
            {
                die($this->Connection->error);
            }
            //Return it
            return $rows->fetch_assoc();
    }

    public function RequestSelect($db, $table, $item) {

        //Selects the database.
        $this->Connection->select_db($db);

        //Escape any sql queries inside the strings to make sure noone tries to inject bad code.
        $etable = mysql_escape_string($table);
        $eitem = mysql_escape_string($item);

        //Get the Data
        $rows = $this->Connection->query("SELECT " . $eitem . " FROM " . $etable);
        if ($this->Connection->error)
        {
            die($this->Connection->error);
        }
        //Return it
        return $rows->fetch_assoc();
    }

    protected function __construct($conn) {
        $this->Connection = $conn;
    }
}

?>
