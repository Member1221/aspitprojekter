<?php include 'backend/dbconn.php'; include 'backend/user.php'; session_start(); ?>
<!DOCTYPE html>
<html>
    <header>
        <title>Seaworld</title>
        <meta charset="utf-8">
        <link rel="stylesheet" type="text/css" href="index.css">
    </header>

    <body class="babyblue">
            <div class="total align_center">
                <div class="logo cyan">
                    <img src="cdn/logo.gif">
                </div>
                <div class="menu cyan">
                        <a href="?pg=planlaegning"><div class="btn"><div class="align_center_t"><center>Planlægning</center></div></div></a>
                        <a href="?pg=brochurer"><div class="btn"><div class="align_center_t"><center>Brochurer</center></div></div></a>
                        <a href="?pg=restaurant"><div class="btn"><div class="align_center_t"><center>Restaurant</center></div></div></a>
                        <a href="?pg=udstillinger"><div class="btn"><div class="align_center_t"><center>Udstillinger</center></div></div></a>
                        <a href="?pg=aktiviteter"><div class="btn"><div class="align_center_t"><center>Aktiviteter</center></div></div></a>
                    <a href="?pg=main"><div class="btn"><div class="align_center_t"><center>Forside</center></div></div></a>
                </div>
                <div class="banner">
                    <img src="cdn/banner.jpg"></img>
                </div>

                <div class="ccontainer">
                    <div class="sidebar cyan">
                        <div class="filledtext">ÅBNINGSTIDER</div>
                        <br>
                        <div class="sidebartext">
                            <strong>Juni - August</strong>
                            <br>
                            Alle dage 10-18<br>
                            Onsdage 10-20
                            <p //>
                            <strong>September - Maj</strong>
                            <br>
                            Alle dage 10-17<br>
                            Onsdage 10-20
                            <p>
                        </div>
                        <p //>
                        <div class="filledtext">DAGENS PROGRAM</div>
                        <br>
                        <div class="sidebartext">
                            10:00 Nordic SeaWorld Åbner<br>
                            11:00 Hajerne fodres<br>
                            13:00 Delfinshow<br>
                            15:00 Hajerne fodres<br>
                            16:00 Svømning med pingviner<br>
                            17:00 Nordic SeaWorld lukker
                        </div>
                        <p //>
                        <div class="filledtext">ADDRESSE</div>
                        <br>
                        <div class="sidebartext">
                            Nordic Seaworld<br>
                            Havnegade 10<br>
                            DK-8000 Århus<br>
                            <p //>
                            Tlf: 86 10 10 10<br>
                            Email: info@nordicseaworld.dk
                            <p //>
                            <a class="fwaylink" href="findvej.php">>>Find vej</a>
                        </div>
                        <p>
                    </div>
                    <div class="content myeyesareburning">
                        <?php $r = $GLOBALS['DB_CONN']->GetOrdered("nordicseaworld", "posts", "posted_at", True);
                        while($row = $r->fetch_assoc())
                        {
                            if (isset($_GET['pg']) && $row['tag'] == $_GET['pg']) {
                                if (isset($_SESSION['USER'])) {
                                    echo "<div title=\"Post UUID: " . $row['uuid'] . "\">";
                                }

                                echo $row['text_html'] . "<br>";
                                if (isset($_SESSION['USER'])) {
                                    echo "</div>";
                                }
                            }
                            else if (!isset($_GET['pg'])){
                                header("Location: ?pg=main");
                            }
                        }
                        ?>
                    </div>

                </div>

            </div>
            <div class="align_center footer rblue">
                <center>Copyright (c) 2016 Nordic SeaWorld All Rights Reserved.</center>
            </div>
    </body>
</html>
