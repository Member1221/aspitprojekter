<?php
include '../backend/user.php';

if (isset($_POST['doregister']))
{
    if (isset($_POST['username']) && $_POST['username'] != "")
    {
        if (isset($_POST['password']) && $_POST['password'] != "")
        {
            //Send a log in request.
            $l = User::NewUser($_POST['username'], $_POST['password']);

            //Did the login return a session? if yes then you're logged in!
            if (isset($l))
            {
                echo "User created!";

                $l = User::Login($_POST['username'], $_POST['password']);

                //Did the login return a session? if yes then you're logged in!
                if (isset($l))
                {
                    header("Location: ..");
                }
                else
                {
                    echo "An unexpected error happened while logging in.";
                }
            }
            else
            {
                echo "Username or password incorrect.";
            }
        }
        else
        {
            echo "Invalid password \"\"!";
        }
    }
    else
    {
        echo "Invalid username \"\"!";
    }
}
else
{?>
    <strong>Registering</strong>
    <form action="register.php" method="post">
        <input type="text" name="username" placeholder="Brugernavn">
        <input type="password" name="password" placeholder="Adgangskode">
        <input type="submit" name="doregister" value="Registrér">
    </form>
<?php
}
?>
