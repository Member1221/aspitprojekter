<?php
include '../backend/dbconn.php';
include '../backend/user.php';
include '../backend/post.php';
session_start();
?>
<!DOCTYPE html>
<html>
<body>
    <?php
    if (isset($_SESSION['USER']))
    {
        if (isset($_POST['doedit']))
        {
            $r = $GLOBALS['DB_CONN']->UpdateTo("nordicseaworld", "posts", "text_html", $_POST['text'], $_GET['id']);
            header("Location: ?id=" . $_GET['id']);
        } else if (isset($_GET['id'])) {
            $r = $GLOBALS['DB_CONN']->RequestAllSpecific("nordicseaworld", "posts", "uuid", $_GET['id']);
            ?>
            <strong>Redigér Post</strong>
            <form <?php echo "action=\"edit.php?id=" . $_GET['id'] . "\""; ?> method="post">
                <input type="text" name="tag" <?php echo "value=\"" . $r['tag'] . "\""; ?> placeholder="side-tag"><br>
                <textarea type="text" rows="16" cols="64" name="text" placeholder="Post Body"><?php echo htmlspecialchars($r['text_html']); ?> </textarea><br>
                <input type="submit" name="doedit" value="Opdatér">
            </form>

        <?php
        }
    } else {
        die("Not logged in.");
    }
?>
</body>
</html>
