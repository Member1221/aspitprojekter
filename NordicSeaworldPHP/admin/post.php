<?php
include '../backend/dbconn.php';
include '../backend/user.php';
include '../backend/post.php';
session_start();
if (isset($_SESSION['USER']))
{
    if (!isset($_POST["dopost"]))
    { ?>
        <strong>Skriv Post</strong>
        <form action="post.php" method="post">
            <input type="text" name="tag" value="main" placeholder="side-tag"><br>
            <textarea type="text" rows="16" cols="64" name="text" placeholder="Post Body"></textarea><br>
            <input type="submit" name="dopost" value="Post">
        </form>
        <?php
    }
    else
    {
        $r = Post::NewPost($_POST["text"], $_POST["tag"]);
        if (isset($r))
        {
            header("Location: ..");
        }
    }
}
?>
