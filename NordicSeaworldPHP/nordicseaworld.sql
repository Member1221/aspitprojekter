-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 09, 2017 at 01:59 PM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `nordicseaworld`
--

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `uuid` varchar(255) NOT NULL,
  `tag` text CHARACTER SET utf8 COLLATE utf8_danish_ci NOT NULL,
  `posted_at` datetime NOT NULL,
  `poster_id` text NOT NULL,
  `text_html` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='A post on the website';

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`uuid`, `tag`, `posted_at`, `poster_id`, `text_html`) VALUES
('58c14157e3283', 'main', '2017-03-09 12:49:43', '58c1153973f0d', '<h1>Velkommen til Nordic SeaWorld</h1>\r\n<br>\r\n<div class=\"float_left\" style=\"width: 360px;\">En tur pÃ¥ Nordic SeaWorld giver altid nye oplevelser. Alene vores tropiske akvarium i det store skibsvrag er et besÃ¸g vÃ¦rd. Du fÃ¥r nÃ¦rmest oplevelsen af, at du er inde i et skib, som er stÃ¸dt pÃ¥ grund, og i havet kan du se koralrev, planter, eksotiske fisk, havskildpadder, hvidhajer og meget andet.</div>\r\n<object class=\"float_right\" data=\"cdn/banner.swf\" width=\"220\" height=\"190\"></object>\r\n  <br><div class=\"end_float\"></div><br><br><br><br><br>\r\n<div class=\"titles\">Nordeuropas stÃ¸rste aktiviteter og udstillinger...</div>\r\nNordic SeaWorld Ã¸nsker at skabe intense maritime oplevelser for dig. Nordic SeaWorld er i den henseende et dynamisk og spÃ¦ndende oplevelsesunivers med og omkring havet, hvor du dels har en unik oplevelse og dels opnÃ¥r ny viden om livet i havet.<br>  '),
('58c1440e26153', 'main', '2017-03-09 13:01:18', '58c1153973f0d', '<div class=\"float_left\"><img src=\"cdn/skibsvrag.jpg\"></div>\r\n<div class=\"slight_padding\" style=\"width: 600px;\">Nordic SeaWorld er for hele familien. De mange udstillinger giver dig lÃ¦rerige og spÃ¦ndende oplysninger om livet under havets overflade, og du kommer du helt tÃ¦t pÃ¥ dyr og planter.Vores daglige aktiviteter med delfiner, pingviner og hajer giver dig en oplevelse for livet.<p>\r\n\r\nNordic SeaWorld holder Ã¥ben til klokken 20 hver onsdag: SpÃ¦ndende foredrag, masser af aktiviteter, lÃ¦kker mad, klap havdyr i sanseakvariet!\r\n</div>    '),
('58c149c2231e9', 'main', '2017-03-09 13:25:38', '58c1153973f0d', '<br><div class=\"titles\">Nyheder</div>\r\nDen enorme kongekrabbe er kommet til Nordic SeaWorld. Med en vÃ¦gt pÃ¥ op til 15 kg og et benspÃ¦nd pÃ¥ mere end to meter er denne krabat et af verdens allerstÃ¸rste krebsdyr. Som det eneste sted i Danmark, udstiller Nordic SeaWorld nu seks eksemplarer af kÃ¦mpen.<br>\r\n<div class=\"end_float\">\r\n<br>\r\n<div class=\"float_left\" style=\"width: 423px;\">\r\nI skolernes sommerferie uge 30 og 31 kÃ¸rer vi temaaktiviteter under overskriften \'Bidt af hajer\'. I de to uger vil der vÃ¦re mange ekstraaktiviteter og oplevelser. Bliv klogere pÃ¥ hvilke hajer, der lever i NordsÃ¸en og hvor store de kan blive. Der vil vÃ¦re ekstra fokus pÃ¥ hajerne under fodringen og hele familien kan dyste om viden om NordsÃ¸ens hajer i en familiequiz. Der bliver ogsÃ¥ mulighed for at rÃ¸re ved forskellige hajer fra NordsÃ¸en â€“ heriblandt en 2 meter lang sildehaj. \r\n</div>\r\n<div class=\"float_left slight_padding\"><img src=\"cdn/haj1.jpg\"></div>');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `username` text CHARACTER SET latin1 COLLATE latin1_danish_ci NOT NULL,
  `password_hash` tinytext NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password_hash`) VALUES
('58c1153973f0d', 'test', '$2y$10$i1M35UqLf/7P9/0zWYGeK.dpG1QnVC0BKHu1zlth/pv7QaDp2X7Xm');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD UNIQUE KEY `uuid` (`uuid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD UNIQUE KEY `id` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
