<?php
include '../backend/user.php';
session_start();

if (!isset($_SESSION['USER']))
{
    if (isset($_POST['dologin']))
    {
        if (isset($_POST['username']) && $_POST['username'] != "")
        {
            if (isset($_POST['password']) && $_POST['password'] != "")
            {
                //Send a log in request.
                $l = User::Login($_POST['username'], $_POST['password']);

                //Did the login return a session? if yes then you're logged in!
                if (isset($l))
                {
                    header("Location: ../index.php");
                }
                else
                {
                    echo "Username or password incorrect.";
                }
            }
            else
            {
                echo "Invalid password \"\"!";
            }
        }
        else
        {
            echo "Invalid username \"\"!";
        }
    }
    else
    {?>
        <strong>Log Ind</strong>
        <form action="login.php" method="post">
            <input type="text" name="username" placeholder="Brugernavn">
            <input type="password" name="password" placeholder="Adgangskode">
            <input type="submit" name="dologin" value="Log Ind">
        </form>
        <?php
    }
}
else
{
    header("Location: ../index.php");
}
?>
